Hi there! :wave_tone1:

My name is Thomas Randolph, and I work on the Code Review part of GitLab (Merge Requests, Diffs, and Merge Request Reviews).

### My Projects

You can find many of the things I've done at GitLab in [my personal to-do project](https://gitlab.com/thomasrandolph/todo/-/issues/11) (and the annual archives!), which tracks my assigned issues, merge requests, and other notes.  
While this doesn't cover everything I do at GitLab, it's a significant portion of it!

You can see my personal projects at [my non-work profile](https://gitlab.com/rockerest).

### Advice

You can leave any advice you might have for me by going to [my personal advice page](https://gitlab-feedback.thomasrandolph.dev).  
It's anonymous by default, but you can choose to identify yourself! Let me know what you think!

### How I review

You can read more about my code review philosophy in [A Good Code Review](https://gitlab.com/-/snippets/2571998).  

### How I work

#### Metadata about my own contributions

You might find comments, commits, MRs, or other data within GitLab where I close the submission with a tag like:

```t:pd#5,wt#tel```

This is me "tagging" (`t:`) comments for later retrieval and organization.

Here are some of my tags:

| Tag | Value | Description |
| --- | ----- | ----------- |
| pd  | `-1` | The work in question / planned approach does not meet the baseline for my Professional Development standards. This is usually due to policy, standard practices, or other misaligned requirements. This type of work spawns exponentially more work and cognitive load due to inherent and deep technical debt. |
| pd  | `0` | The work in question / planned approach only meets the minimum baseline for my Professional Development standards. While technically "acceptable," this work could be dramatically better. This is usually due to policy, standard practices, time constraints, or other misaligned requirements. This work will still spawn compounded additional work due to inherent technical debt. |
| pd  | `1-n` | The work in question / planned approach exceeds the bare minimum for my Professional Development standards by some factor (1-n). The higher this number, the less likely this code is to cause significant issues in the future. |
| ty  | `tel` | This Work Type is telemetry and/or instrumentation. |